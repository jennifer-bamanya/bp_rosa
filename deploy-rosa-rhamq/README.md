#### AMQ-BROKER-OPERATOR Deployment

This helm chart installs amq-broker-operator and manages the amq-broker instance.

The chart will be deployed on ROSA (OCP4) using Jenkins CI/CD
